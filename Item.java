package com.basic.day4;

public class Item {
	private int id;
	private String description;
	private double price;

	public Item() {
		this(1,"",23);
		System.out.println("Item default constructor");
	}
	public Item(int id,  String description, double price) {
		//this();
		System.out.println("Item param");
		this.id = id;

		this.description = description;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}


}


