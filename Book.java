package com.basic.day4;

public class Book extends Item {
	private String title;
	private String author;
	public Book(int id, String description, double price,String title,String author) {
		super(id,description,price);
		System.out.println("Book param constructor");
		// TODO Auto-generated constructor stub
		this.title = title;
		this.author = author;
	}
	@Override
	public String toString() {
		return "Book [title=" + title + ", author=" + author + ", getId()=" + getId() + ", getDescription()="
				+ getDescription() + ", getPrice()=" + getPrice() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	

}
