package com.basic.day4;

public class TestCommon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp = new Employee();

		System.out.println(emp.dailyTask());

		System.out.println(emp.markAttendance());

		System.out.println(emp.displayDetails());

		Manager manager = new Manager();

		System.out.println(manager.markAttendance());

		System.out.println(manager.dailyTask());

		System.out.println(manager.displayDetails());

	}

}
