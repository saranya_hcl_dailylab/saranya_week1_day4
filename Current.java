package com.basic.day4;

public class Current extends Account {
	/**
	 * ii)CurrentAccount class will extend Account class

    Declare a variable currentBalance (of type int)

  	CurrentAccount will contain a parameterized constructor  CurrentAccount(String a, int b, int c)

	Use super keyword inside Constructor to access Account constructor and assign currentBalance to c.

	Create a method display of type void to display (customerName, AccountNo, CurrentBalance)

	Inside display method use super keyword to display customerName and accountNo
	 */
	int currentBalance;

	public Current(String customerName, int accountNo, int currentBalance) {
		super(customerName, accountNo);
		this.currentBalance = currentBalance;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		
		System.out.println(currentBalance);
		super.display();
	}
	
}
