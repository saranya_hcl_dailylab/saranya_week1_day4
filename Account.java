package com.basic.day4;

public class Account {
	/**
	 * Write a program in Java to implement Multilevel inheritance.
	   Create three Classes Account, CurrentAccount, AccountDetails.
	i) Account Class is the super Class
    Declare two variables customerName (of type String), accountNo(of type int)

    Account class will contain a parameterized constructor Account(String a, int b)

    Inside Constructor Assign parameter values to the variables declared 

    Create a method display of type void to display customerName and accountNo

    */
	protected String customerName;
	protected int accountNo;
	
	public Account(String customerName,int accountNo){
		this.customerName = customerName;
		this.accountNo=accountNo;
	}
	public void display() {
		System.out.println(customerName +" "+accountNo);
	}

}
